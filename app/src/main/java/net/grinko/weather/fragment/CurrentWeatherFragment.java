package net.grinko.weather.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.grinko.weather.R;
import net.grinko.weather.activity.MainActivity;
import net.grinko.weather.model.Weather;
import net.grinko.weather.model.WeatherLocation;
import net.grinko.weather.utils.Utils;

/**
 * Created by Nick on 2/22/2015.
 */
public class CurrentWeatherFragment extends Fragment {
    private TextView mLocationTextView;
    private ImageView mWeatherImageView;
    private TextView mTempTextView;
    private WeatherLocation mWeatherLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_current, container, false);
        mLocationTextView = (TextView) view.findViewById(R.id.weather_location);
        mWeatherImageView = (ImageView) view.findViewById(R.id.weather_icon);
        mTempTextView = (TextView) view.findViewById(R.id.weather_temp);

        //setup views
        Bundle args = getArguments();
        if (args != null) {

            mWeatherLocation = (WeatherLocation) args.getSerializable(MainActivity.WEATHER_LOCATION_KEY);
            mLocationTextView.setText(mWeatherLocation.getName());
            Weather currentWeather = mWeatherLocation.getCurrentWeather();
            Picasso.with(getActivity()).load(currentWeather.getWeatherIconUrl())
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_launcher).into(mWeatherImageView);
            boolean showF = Utils.getSharedPrefs().getBoolean(MainActivity.TEMPERATURE_SETTING_KEY, true);

            StringBuilder builder = new StringBuilder(currentWeather.getWeatherDesc());
            builder.append(" ");
            if (showF) {
                builder.append(currentWeather.getTempF()).append(" F");
            } else {
                builder.append(currentWeather.getTempC()).append(" C");
            }

            mTempTextView.setText(builder.toString());
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWeatherLocation != null) {

                    ListWeatherFragment f = new ListWeatherFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(MainActivity.WEATHER_LOCATION_KEY, mWeatherLocation);
                    f.setArguments(args);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_from_bottom, R.anim.slide_from_top,
                                    R.anim.slide_from_bottom, R.anim.slide_from_top)
                            .replace(R.id.fragment_container, f).commit();
                }
            }
        });
        return view;
    }
}