package net.grinko.weather.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.grinko.weather.R;
import net.grinko.weather.activity.MainActivity;
import net.grinko.weather.model.Day;
import net.grinko.weather.model.Weather;
import net.grinko.weather.model.WeatherLocation;
import net.grinko.weather.utils.Utils;

/**
 * Created by Nick on 2/22/2015.
 */
public class ListWeatherFragment extends Fragment {

    private WeatherLocation mWeatherLocation;
    private DaysAdapter mDaysAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        ListView listView = (ListView) inflater.inflate(R.layout.fragment_list, container, false);
        Bundle args = getArguments();
        if (args != null) {
            mWeatherLocation = (WeatherLocation) args.getSerializable(MainActivity.WEATHER_LOCATION_KEY);
            mDaysAdapter = new DaysAdapter();
            listView.setAdapter(mDaysAdapter);
        }
        return listView;
    }

    //ListView Adapter
    private class DaysAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return (mWeatherLocation == null || mWeatherLocation.getDays() == null) ?
                    0 : mWeatherLocation.getDays().size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LVH vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item, parent, false);
                vh = new LVH(convertView);
                vh.recycler.setLayoutManager(new HorizontalListRecyclerLayoutManager(getActivity()));
                vh.recycler.setHasFixedSize(true);
                vh.recycler.setItemViewCacheSize(0);
                convertView.setTag(vh);
            }
            vh = (LVH) convertView.getTag();

            Day day = mWeatherLocation.getDays().get(position);
            vh.date.setText(day.getDate());

            vh.recycler.setAdapter(new DayAdapter(day));
            return convertView;
        }

        private class LVH {
            public TextView date;
            public RecyclerView recycler;

            public LVH(View view) {
                date = (TextView) view.findViewById(R.id.date);
                recycler = (RecyclerView) view.findViewById(R.id.recycler);
            }
        }
    }

    //RecyclerView Adapter
    private class DayAdapter extends RecyclerView.Adapter<RVH> {

        private Day mDay;

        public DayAdapter(Day day) {
            mDay = day;
        }

        @Override
        public RVH onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
            return new RVH(view);
        }

        @Override
        public void onBindViewHolder(RVH holder, int position) {
            Weather weather = mDay.get(position);
            Picasso.with(getActivity()).load(weather.getWeatherIconUrl())
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_launcher).into(holder.image);
            boolean showF = Utils.getSharedPrefs().getBoolean(MainActivity.TEMPERATURE_SETTING_KEY, true);

            StringBuilder builder = new StringBuilder(weather.getTime());
            builder.append("\n").append(weather.getWeatherDesc()).append("\n");
            if (showF) {
                builder.append(mDay.get(position).getTempF()).append(" F");
            } else {
                builder.append(mDay.get(position).getTempC()).append(" C");
            }

            holder.temp.setText(builder.toString());
        }

        @Override
        public int getItemCount() {
            return mDay == null ? 0 : mDay.size();
        }
    }

    //LayoutManager to make recyclerView scroll horizontally
    public class HorizontalListRecyclerLayoutManager extends LinearLayoutManager {

        public HorizontalListRecyclerLayoutManager(Context context) {
            this(context, LinearLayoutManager.HORIZONTAL, false);
        }

        public HorizontalListRecyclerLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        @Override
        public RecyclerView.LayoutParams generateDefaultLayoutParams() {
            return new RecyclerView.LayoutParams(
                    RecyclerView.LayoutParams.WRAP_CONTENT,
                    RecyclerView.LayoutParams.WRAP_CONTENT);
        }
    }

    //view holder for RecyclerView
    private class RVH extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView temp;

        public RVH(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.weather_icon);
            temp = (TextView) itemView.findViewById(R.id.weather_temp);
        }
    }
}
