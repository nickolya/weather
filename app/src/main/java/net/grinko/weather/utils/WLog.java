package net.grinko.weather.utils;

import android.util.Log;

/**
 * Created by Nick on 2/20/2015.
 */
public class WLog {
    private static final String TAG = "ecoATM: ";
    private static final boolean DEBUG_MODE = true;

    public static void e(String tag, String msg) {
        if (DEBUG_MODE) {
            Log.e(TAG + tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (DEBUG_MODE) {
            Log.w(TAG + tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (DEBUG_MODE) {
            Log.i(TAG + tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (DEBUG_MODE) {
            Log.d(TAG + tag, msg);
        }
    }
}
