package net.grinko.weather.utils;

import android.net.Uri;

/**
 * Created by Nick on 2/20/2015.
 */
public class APIContract {

    private static final String BASE_URI = "http://api.worldweatheronline.com/free/v2/weather.ashx";


    public static final String getWeatherUrl(String location) {
        return Uri.parse(BASE_URI).buildUpon()
                .appendQueryParameter("q", location)
                .appendQueryParameter("format", "json")
                .appendQueryParameter("num_of_days", "5")
                .appendQueryParameter("key", "0ae26e2bce45b635819eb94d22730")
                .build().toString();
    }
}
