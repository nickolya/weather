package net.grinko.weather.application;

import android.app.Application;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

/**
 * Created by Nick on 2/20/2015.
 */
public class WeatherApp extends Application {


    private RequestQueue mRequestQueue;
    private static WeatherApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = WeatherApp.this;
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();

    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public static WeatherApp getInstance() {
        return mInstance;
    }
}
