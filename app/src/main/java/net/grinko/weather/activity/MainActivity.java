package net.grinko.weather.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import net.grinko.weather.R;
import net.grinko.weather.application.WeatherApp;
import net.grinko.weather.fragment.CurrentWeatherFragment;
import net.grinko.weather.fragment.ListWeatherFragment;
import net.grinko.weather.model.Days;
import net.grinko.weather.model.Weather;
import net.grinko.weather.model.WeatherLocation;
import net.grinko.weather.utils.APIContract;
import net.grinko.weather.utils.Utils;
import net.grinko.weather.utils.WLog;

import org.json.JSONArray;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, SearchView.OnQueryTextListener, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String TEMPERATURE_SETTING_KEY = "use_fahrenheit_key";
    private static final String TAG = "MainActivity";
    private static final long FASTEST_INTERVAL = 5 * 1000;
    private static final long INTERVAL = 30 * 1000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 111;
    private static final String TAG_GET_WEATHER = "GetWeather";

    public static final String WEATHER_LOCATION_KEY = "WeatherLocationKey";
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    private GoogleApiClient mGoogleApiClient;
    private SearchView mSearchView;
    private ImageView mMyLocation;
    private RequestQueue mRequestQueue;
    private CurrentWeatherFragment mCurrentWeatherFragment;
    private ListWeatherFragment mListWeatherFragment;
    //    private ViewGroup mFragmentContainer;
    private WeatherLocation mWeatherLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //start request queue
        mRequestQueue = WeatherApp.getInstance().getRequestQueue();

        //setup views
        mSearchView = (SearchView) findViewById(R.id.search);
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setQueryHint(getString(R.string.search_hint));
        mSearchView.setOnQueryTextListener(this);

        mMyLocation = (ImageView) findViewById(R.id.my_location);
        mMyLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updateCurrentLocation();
            }
        });

        if (savedInstanceState != null) {
            mWeatherLocation = (WeatherLocation) savedInstanceState.getSerializable(WEATHER_LOCATION_KEY);
        } else {

            //check network connection. Restore if possible
            checkConnection();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putSerializable(WEATHER_LOCATION_KEY, mWeatherLocation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            showUnitDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    //simple
    private void showUnitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog = builder.setMessage(getString(R.string.units_dialog_message))
                .setCancelable(true)
                .setPositiveButton(R.string.fahrenheit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.getSharedPrefs().edit().putBoolean(TEMPERATURE_SETTING_KEY, true).commit();
                            }
                        }).start();
                    }
                })
                .setNegativeButton(R.string.celsius, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.getSharedPrefs().edit().putBoolean(TEMPERATURE_SETTING_KEY, false).commit();
                            }
                        }).start();
                    }
                })
                .create();


        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideKeyboard(mSearchView);
    }

    //check network connection method. Allow user to retry
    private void checkConnection() {
        if (isNetworkDisconnected()) {
            showErrorDialog(getRetryListener(), R.string.retry_btn, true);
        } else {
            onConnected();
        }
    }


    //update current location once network is available
    void onConnected() {

        if (checkPlayServices()) {

            //init location service
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }

            if (mWeatherLocation == null) {
                updateCurrentLocation();
            }
        }
    }

    public boolean checkPlayServices() {

        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode == ConnectionResult.SUCCESS) {
            WLog.d(TAG, "Google Play Service: Version: "
                    + GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
            return true;
        } else {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                WLog.w(TAG, "Google Play Service: Version: NOT CURRENT "
                        + GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                WLog.e(TAG, "Google Play Services: This device is not supported.");
                finish();
            }
            return false;
        }
    }


    private OnNetworkErrorClickListener getRetryListener() {
        return new OnNetworkErrorClickListener() {

            @Override
            public void onButtonClick() {
                checkConnection();
            }
        };
    }

    //helper to check if network is disconnected
    public boolean isNetworkDisconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo == null || !netInfo.isConnected());
    }

    public void showErrorDialog(OnNetworkErrorClickListener listener, int buttonTitle,
                                final boolean finishOnCancel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog = builder.setMessage(getString(R.string.no_connection_dialog_message))
                .setCancelable(true)
                .setPositiveButton(buttonTitle, listener)
                .create();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                if (finishOnCancel) {
                    finish();
                }
            }
        });

        dialog.show();
    }


    //method to get current location and search for weather in this location
    //if location is not available or client is not connected - try to restore
    private void updateCurrentLocation() {
        WLog.d(TAG, "Update current location");
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {

                Location currentLocation = fusedLocationProviderApi
                        .getLastLocation(mGoogleApiClient);
                WLog.d(TAG, "Update location. Current location is "
                        + currentLocation);
                if (currentLocation != null) {
                    WLog.d(TAG, "GoogleApiClient connected. Current location is updated");
                    search(currentLocation);
                } else {
                    WLog.d(TAG, "GoogleApiClient connected. Current location is NOT updated.");

                    if (isLocationEnabled()) {
                        WLog.d(TAG, "GoogleApiClient connected. Show enable dialog.");
                        showEnableLocationsDialog();
                    } else {
                        WLog.d(TAG, "GoogleApiClient connected. Start request.");
                        requestLocation();
                    }
                }
            } else if (!mGoogleApiClient.isConnecting()) {
                WLog.d(TAG, "Update current. Google Api Client should be connected.");
                mGoogleApiClient.connect();
            }
        }
    }

    //request location using fused location provider
    private void requestLocation() {

        LocationRequest locationRequest = LocationRequest
                .create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        fusedLocationProviderApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    //once provider returned detected location
    @Override
    public void onLocationChanged(Location location) {
        WLog.d(TAG, "Location Listener. Location changed");
        search(location);

        // unsubscribe listener
        fusedLocationProviderApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        WLog.d(TAG, "Search submit: Query " + s);
        search(s);
        Utils.hideKeyboard(mSearchView);
        mSearchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    //search for weather using current location (cannot be null)
    private void search(Location location) {
        String query = location.getLatitude() + "," + location.getLongitude();
        search(query);
    }

    //search for weather using search query
    private void search(String query) {
        WLog.d(TAG, "Search by query. Query is " + query);
        mRequestQueue.cancelAll(TAG_GET_WEATHER);
        String url = APIContract.getWeatherUrl(query);
        JsonObjectRequest getDeviceCategories = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        WLog.d(TAG, "Loading weather finished. Response = " + response);
                        if (response != null) {
                            mCurrentWeatherFragment = null;
                            mListWeatherFragment = null;
                            parseWeather(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        WLog.w(TAG, "Handle error: " + error);
                        showErrorDialog(new OnNetworkErrorClickListener() {

                            @Override
                            public void onButtonClick() {
                                finish();
                            }
                        }, R.string.ok_btn, true);
                    }
                });
        getDeviceCategories.setTag(TAG_GET_WEATHER);
        mRequestQueue.add(getDeviceCategories);
        WLog.d(TAG, "Start loading weather. Query url = " + url);

    }

    //parse response and save as weather location object
    private void parseWeather(JSONObject response) {
        Weather currentWeather;
        Days days;
        String query = null;
        try {

            JSONObject data = response.optJSONObject("data");
            if (data == null) {
                return;
            }
            JSONArray currentConditionsArray = data.optJSONArray("current_condition");
            JSONArray requestArray = data.optJSONArray("request");
            JSONArray daysArray = data.optJSONArray("weather");
            if (currentConditionsArray == null || requestArray == null || daysArray == null ||
                    currentConditionsArray.length() <= 0 || requestArray.length() <= 0 || daysArray.length() <= 0) {
                return;
            }
            query = requestArray.getJSONObject(0).optString("query");
            String type = requestArray.getJSONObject(0).optString("type");
            if ("LatLon".equals(type)) {
                query = "Current Location";
            }

            currentWeather = Weather.createFromJSONObject(currentConditionsArray.getJSONObject(0));
            days = Days.createFromJSONArray(daysArray);

        } catch (Exception e) {
            e.printStackTrace();
            currentWeather = null;
            days = null;
        }

        if (query != null && currentWeather != null && days != null) {
            mWeatherLocation = new WeatherLocation(query, currentWeather, days);
            mCurrentWeatherFragment = new CurrentWeatherFragment();
            Bundle args = new Bundle();
            args.putSerializable(WEATHER_LOCATION_KEY, mWeatherLocation);
            mCurrentWeatherFragment.setArguments(args);

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mCurrentWeatherFragment).commit();
        }
    }

    //helper to check if location service is enabled
    private boolean isLocationEnabled() {
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    private void showEnableLocationsDialog() {

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.location_services_disabled))
                .setCancelable(true)
                .setPositiveButton(R.string.enable_btn,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                .create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                requestLocation();

            }
        });
        dialog.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        updateCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static abstract class OnNetworkErrorClickListener implements
            DialogInterface.OnClickListener {
        public abstract void onButtonClick();

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            onButtonClick();
        }
    }
}
