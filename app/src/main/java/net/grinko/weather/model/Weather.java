package net.grinko.weather.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Nick on 2/20/2015.
 */
public class Weather implements Serializable {

    @JSONKey("FeelsLikeC")
    private String feelsLikeC;
    @JSONKey("FeelsLikeF")
    private String feelsLikeF;
    @JSONKey("humidity")
    private String humidity;
    @JSONKey("pressure")
    private String pressure;
    @JSONKey("temp_C")
    private String temp_C;
    @JSONKey("temp_F")
    private String temp_F;
    @JSONKey("tempC")
    private String tempC;
    @JSONKey("tempF")
    private String tempF;
    @JSONKey("time")
    private String time;

    private String weatherDesc;
    private String weatherIconUrl;

    public static Weather createFromJSONObject(JSONObject jsonObject) throws Exception {
        Weather weather = JSONHelper.createObjectFromJSON(Weather.class, jsonObject);
        weather.setWeatherDesc(jsonObject.getJSONArray("weatherDesc").getJSONObject(0).getString("value"));
        weather.setWeatherIconUrl(jsonObject.getJSONArray("weatherIconUrl").getJSONObject(0).getString("value"));

        return weather;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public void setWeatherIconUrl(String weatherIconUrl) {
        this.weatherIconUrl = weatherIconUrl;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public void setWeatherDesc(String weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public String getFeelsLikeC() {
        return feelsLikeC;
    }

    public void setFeelsLikeC(String feelsLikeC) {
        this.feelsLikeC = feelsLikeC;
    }

    public String getFeelsLikeF() {
        return feelsLikeF;
    }

    public void setFeelsLikeF(String feelsLikeF) {
        this.feelsLikeF = feelsLikeF;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getTempC() {
        return tempC == null ? temp_C : tempC;
    }

    public void setTempC(String tempC) {
        this.tempC = tempC;
    }

    public String getTempF() {
        return tempF == null ? temp_F : tempF;
    }

    public void setTempF(String tempF) {
        this.tempF = tempF;
    }
}