package net.grinko.weather.model;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Nick on 2/22/2015.
 */
public class Days extends ArrayList<Day> {

    public static Days createFromJSONArray(JSONArray jsonArray) throws Exception {
        Days days = new Days();

        if (jsonArray != null && jsonArray.length() > 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                Day day = Day.createFromJSONObject(jsonArray.getJSONObject(i));
                days.add(day);
            }
        }
        return days;
    }
}
