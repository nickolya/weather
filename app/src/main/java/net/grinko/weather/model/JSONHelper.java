package net.grinko.weather.model;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.ParseException;

public class JSONHelper {

    public static <T> T createObjectFromJSON(final Class<T> clazz, final JSONObject jsonObject)
            throws IllegalAccessException, InstantiationException, ParseException {
        final T instance = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields())
            if (field.isAnnotationPresent(JSONKey.class)) {
                field.setAccessible(true);
                final JSONKey jsonKey = field.getAnnotation(JSONKey.class);
                Object value = jsonObject.opt(jsonKey.value());
                if (value != null && !JSONObject.NULL.equals(value)) {
                    field.set(instance, value);
                }
            }
        return instance;
    }


}
