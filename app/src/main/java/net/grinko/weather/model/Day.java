package net.grinko.weather.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nick on 2/22/2015.
 */
public class Day extends ArrayList<Weather> {

    @JSONKey("date")
    private String date;

    public static Day createFromJSONObject(JSONObject jsonObject) throws Exception {
        Day day = JSONHelper.createObjectFromJSON(Day.class, jsonObject);

        JSONArray hourly = jsonObject.optJSONArray("hourly");
        if (hourly != null && hourly.length() > 0) {
            for (int i = 0; i < hourly.length(); i++) {
                Weather weather = Weather.createFromJSONObject(hourly.getJSONObject(i));
                day.add(weather);
            }
        }
        return day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
