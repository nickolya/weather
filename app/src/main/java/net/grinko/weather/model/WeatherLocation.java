package net.grinko.weather.model;

import java.io.Serializable;

/**
 * Created by Nick on 2/22/2015.
 */
public class WeatherLocation implements Serializable {

    private String name;
    private Weather currentWeather;
    private Days days;

    public WeatherLocation(String name, Weather currentWeather, Days days) {
        this.name = name;
        this.currentWeather = currentWeather;
        this.days = days;
    }

    public String getName() {
        return name;
    }

    public Weather getCurrentWeather() {
        return currentWeather;
    }

    public Days getDays() {
        return days;
    }
}
